'use strict';
const faker = require('faker');

module.exports = {
  up: async (queryInterface, Sequelize) => {
    for (let i = 1; i < 16; i++) {
      
      // create random rank based on GamePoint
      const gamePoint = Math.floor(Math.random() * 1500);
      let rank;
      if (gamePoint <= 500) rank = 'Bronze';
      else if (gamePoint <= 1000) rank = 'Silver';
      else rank = 'Gold';

      // generate userid
      let fake_id = faker.datatype.uuid();

      // seeder user_game
      await queryInterface.bulkInsert('user_game', [
        {
          user_id: fake_id,
          username: faker.internet.userName(),
          password: faker.internet.password(),
          game_point: gamePoint,
          rank: rank,
          created_at: new Date(),
          updated_at: new Date(),
        },
      ]);

      // seeder user_game_biodata
      await queryInterface.bulkInsert('user_game_bio', [
        {
          id: faker.datatype.uuid(),
          first_name: faker.name.firstName(),
          last_name: faker.name.lastName(),
          email: faker.internet.email(),
          country: faker.address.countryCode(),
          user_id: fake_id,
          created_at: new Date(),
          updated_at: new Date(),
        },
      ]);

      // create random data , how many times user playing (1-5)
      const matchPlayed = Math.floor(Math.random() * 5);

      // seeder user_game_history
      for (let p = 0; p <= matchPlayed; p++) {
        await queryInterface.bulkInsert('user_game_history', [
          {
            game_instance: faker.datatype.uuid(),
            match_point: Math.floor(Math.random() * 150 + 10),
            user_id: fake_id,
            created_at: new Date(),
            updated_at: new Date(),
          },
        ]);
      }
    }
  },

  down: async (queryInterface, Sequelize) => {
    // deleting parent table will delete other table's record, with cascade on delete
    await queryInterface.bulkDelete('user_game', null, {});
  },
};
