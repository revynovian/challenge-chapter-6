'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    // create table user's biodata
    await queryInterface.createTable('user_game_bio', {
      id: {
        allowNull: false,
        primaryKey: true,
        type: Sequelize.UUID,
      },
      first_name: {
        type: Sequelize.STRING,
      },
      last_name: {
        type: Sequelize.STRING,
      },
      email: {
        type: Sequelize.STRING,
      },
      country: {
        type: Sequelize.STRING,
      },
      user_id: {
        type: Sequelize.UUID,
        allowNull: false,
      },
      created_at: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      updated_at: {
        allowNull: false,
        type: Sequelize.DATE,
      },
    });

    // add foreign key
    await queryInterface.addConstraint('user_game_bio', {
      fields: ['user_id'],
      type: 'foreign key',
      name: 'user_id_fkey',
      references: {
        table: 'user_game',
        field: 'user_id',
      },
      onDelete: 'cascade',
    });
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('user_game_bio');
  },
};
