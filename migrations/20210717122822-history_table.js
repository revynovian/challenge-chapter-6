'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    // create table user's biodata
    await queryInterface.createTable('user_game_history', {
      game_instance: {
        allowNull: false,
        primaryKey: true,
        type: Sequelize.UUID,
      },
      match_point: {
        type: Sequelize.INTEGER,
      },
      user_id: {
        type: Sequelize.UUID,
        allowNull: false,
      },
      created_at: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      updated_at: {
        allowNull: false,
        type: Sequelize.DATE,
      },
    });

    // add foreign key
    await queryInterface.addConstraint('user_game_history', {
      fields: ['user_id'],
      type: 'foreign key',
      name: 'user_id_fkey',
      references: {
        table: 'user_game',
        field: 'user_id',
      },
      onDelete: 'cascade',
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('user_game_history');
  },
};
