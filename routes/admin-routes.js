const express = require('express');
const router = express.Router();
const {loginPage, loginCheck, dashboardPage, usersPage, registerPage, createUser, deleteUser, updateUser} = require('../controllers/admin-controllers');

router.get('/login', loginPage);
router.post('/login', loginCheck);
router.get('/dashboard', dashboardPage);
router.get('/users', usersPage);
router.get('/register', registerPage);
router.post('/create', createUser);
router.get('/delete', deleteUser);
router.post('/update', updateUser);

module.exports = router;