const express = require('express');
const app = express();
const PORT = 3000;
const { sequelize, UserGame, UserGameBio, UserGameHistory } = require('./models');
const router = require('./routes/admin-routes');

app.set('view engine', 'ejs');
app.use(express.static('public'));
app.use(express.urlencoded({extended: false}));

app.use('/admin', router);

// main routes
app.get('/', (req, res) => {
  res.render('index', { title: 'chapter-3' });
});
app.get('/game', (req, res) => {
  res.render('game', { title: 'chapter-4' });
});

// server error handler 5xx
app.use((err, req, res, next) => {
  res.status(500).render('errors', { title: 'error page', status: '500' });
  next();
});
// client error handler 4xx
app.use((req, res, next) => {
  res.status(404).render('errors', { title: 'error page', status: '404' });
});

// check connection to database
sequelize
  .authenticate()
  .then(() => {
    console.log('Connection has been established successfully.');
  })
  .catch((err) => {
    console.error('Unable to connect to the database:', err);
  });

// express server connection
app.listen(PORT, () => console.log(`Server running at http://localhost:${PORT}`));
