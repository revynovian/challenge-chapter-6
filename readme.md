# Chapter 6 - Challenge

### Express Template Engine and ORM (Sequelize)

---

### How to run project

>STEP 1 : install dependencies

`npm run install`

>STEP 2 : make environment variables file (.env) based on .env.example

eg:
| Environtment variables
| :--------------  
|DB_USER=dbuser  
|DB_NAME=dbname
|DB_PASSWORD=dbpass
|DB_HOST=localhost

>STEP 3 : create database

`npm run dbcreate`

>STEP 4 : migrate database schema

`npm run dbmigrate`

>STEP 5 : populate database using seeder

`npm run dbseed`

> STEP 6 : run the app and server will be ready at http://localhost:3000/

`npm run dev`

---

### Available Endpoints

|    Endpoint      |  Description    |
| :--------------  | :------------------- |
|        /         |  Landing Page        |
|      /game       |  Game                |
|  /admin/login    |  login page          |
|  /admin/dashboard|  admin dashboard     |
|  /admin/users    |  user profile page         |
|  /admin/register |  registration page         |
