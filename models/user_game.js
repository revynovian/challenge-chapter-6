'use strict';
const { Model } = require('sequelize');

module.exports = (sequelize, DataTypes) => {
  class UserGame extends Model {
    static associate({ UserGameBio, UserGameHistory }) {
      this.hasOne(UserGameBio, { foreignKey: 'user_id'});
      this.hasMany(UserGameHistory, { foreignKey: 'user_id' });
    }
  }
  UserGame.init(
    {
      user_id: {
        primaryKey: true,
        type: DataTypes.UUID,
        allowNull: false,
      },
      username: DataTypes.STRING,
      password: DataTypes.STRING,
      rank: {
        type: DataTypes.STRING,
        defaultValue: 'Bronze',
      },
      game_point: {
        type: DataTypes.INTEGER,
        defaultValue: 0,
      },
    },
    {
      sequelize,
      modelName: 'UserGame',
      tableName: 'user_game',
      underscored: true,
    }
  );
  return UserGame;
};
