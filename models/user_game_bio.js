'use strict';
const { Model } = require('sequelize');

module.exports = (sequelize, DataTypes) => {
  class UserGameBio extends Model {
    static associate({ UserGame }) {
      this.belongsTo(UserGame, { foreignKey: 'user_id'});
    }
  }
  UserGameBio.init(
    {
      id: {
        primaryKey: true,
        type: DataTypes.UUID,
        allowNull: false,
      },
      first_name: DataTypes.STRING,
      last_name: DataTypes.STRING,
      email: DataTypes.STRING,
      country: DataTypes.STRING,
      user_id: {
        type: DataTypes.UUID,
        references: {
          model: 'user_game',
          key: 'user_id',
        },
      },
    },
    {
      sequelize,
      modelName: 'UserGameBio',
      tableName: 'user_game_bio',
      underscored: true,
    }
  );
  return UserGameBio;
};
